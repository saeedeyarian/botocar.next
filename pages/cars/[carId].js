import { useRouter } from "next/router"
import carsData from "../../data/CarsData"
import CarsDetails from "../../components/template/CarsDetails";

function CarDetails() {

    const router = useRouter();
    const {carId} = router.query 
    console.log(carId)
    const CarDetail = carsData[carId - 1]


  return (
      <CarsDetails {...CarDetail} />
  ) 
  
}

export default CarDetails
