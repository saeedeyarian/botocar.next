import Categories from "../components/module/Categories";
import CarsPage from "../components/template/CarsPage";
import SearchBar from "../components/module/SearchBar";
import carsData from "../data/CarsData";
import AllButton from "../components/module/AllButton";

export default function Home() {

  const car = carsData.slice(0,3)

  return (
    <div>
      <SearchBar/>
      <Categories/>
      <AllButton/>
      <CarsPage data={car} />
    </div>
  )
}
