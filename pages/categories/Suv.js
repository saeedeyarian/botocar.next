import CarsData from "../../data/CarsData";
import CarsList from "../../components/template/CarsList";


function Sedan() {

    const suv = CarsData.filter(car => car.category === "suv")

  return (
    <div>
      <CarsList data={suv}/>
    </div>
  )
}

export default Sedan
