import CarsData from "../../data/CarsData";
import CarsList from "../../components/template/CarsList";


function Sedan() {

    const hatchback = CarsData.filter(car => car.category === "hatchback")

  return (
    <div>
      <CarsList data={hatchback}/>
    </div>
  )
}

export default Sedan
