import CarsData from "../../data/CarsData";
import CarsList from "../../components/template/CarsList";


function Sedan() {

    const sedan = CarsData.filter(car => car.category === "sedan")

  return (
    <div>
      <CarsList data={sedan}/>
    </div>
  )
}

export default Sedan
