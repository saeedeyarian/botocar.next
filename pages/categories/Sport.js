import CarsData from "../../data/CarsData";
import CarsList from "../../components/template/CarsList";


function Sedan() {

    const sport = CarsData.filter(car => car.category === "sport")

  return (
    <div>
      <CarsList data={sport}/>
    </div>
  )
}

export default Sedan
