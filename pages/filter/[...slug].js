import CarsList from "../../components/template/CarsList";
import carsData from "../../data/CarsData";
import { useRouter } from "next/router";

function FilterCards() {

    const router = useRouter()
    const [min,max] = router.query.slug || []
    console.log(router)

    const filterData = carsData.filter (item => item.price > min && item.price < max)

    if(!filterData.length) return <h3>Not found</h3>

  return (
    <div>
      <CarsList data = { filterData }/>
    </div>
  )
}

export default FilterCards
