//component
import Link from "next/link";
//css
import styles from "./Layout.module.scss"

function Layout({children}) {
  return (
    <>
        <headr className={styles.header}>
            <Link href='/'>
                <h2>BOTOCAR</h2>
                <p>choose and buy your car</p>
            </Link>
        </headr>
        <div className={styles.container}>
            {children}
        </div>
        <footer className={styles.footer}>
            Next.js courses | BotoCar Projects
        </footer>
    </>
  )
}

export default Layout
