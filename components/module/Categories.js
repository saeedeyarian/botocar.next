import styles from "./Categories.module.scss";

import Link from "next/link"

//icon
import Sedan from "../../components/icons/Sedan";
import Suv from "../../components/icons/Suv";
import Hatchback from "../../components/icons/Hatchback";
import Sport from "../../components/icons/Sport";

function Categories() {
  return (
    <div className={styles.container}>
        <Link href="/categories/Sedan">
            <div>
                <p>sedan</p>
                <Sedan/>
            </div>
        </Link>
        <Link href="/categories/Suv">
            <div>
                <p>suv</p>
                <Suv/>
            </div>
        </Link>
        <Link href="/categories/Sport">
            <div>
                <p>sport</p>
                <Sport />
            </div>
        </Link>
        <Link href="/categories/Hatchback">
            <div>
                <p>hatchback</p>
                <Hatchback/>
            </div>
        </Link>
    </div>
  )
}

export default Categories
