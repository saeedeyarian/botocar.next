import Link from 'next/link'

//css
import styles from "./Card.module.scss"


//icon
import Location from "../icons/Location";

function Card(props) {

    const {id,name,model,year,distance,location,price,image} = props;

  return (
    <Link href={`/cars/${id}`}>
        <div className={styles.container}>
        <img src={image} className={styles.image}/>
        <h2 className={styles.title}>{`${name}  ${model}`}</h2>
        <p className={styles.detail}>{`${year} . ${distance}`}</p>
        <div className={styles.footer}>
            <p className={styles.price}>{price}</p>
            <div className={styles.location}>
                <p>{location}</p>
                <Location/>
            </div>
        </div>
        </div>
    </Link>
  )
}

export default Card
