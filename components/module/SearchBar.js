import { useState } from "react";
import { useRouter } from "next/router";

import styles from "./SearchBar.module.scss"

function SearchBar() {

    const router = useRouter()

    const [min, setMin] = useState("")
    const [max, setMax] = useState("")

    const searchHandler = () => {
        if(min && max) {
            router.push (`/filter/${min}/${max}`)
        } else {
            alert("please enter price")
        }
    }

  return (
    <div className={styles.container}>
      <div>
        <input 
            placeholder="Enter min-price"
            value={min}
            onChange={e => setMin(e.target.value)}
        />
        <input 
            placeholder="Enter max-price"
            value={max}
            onChange={e => setMax(e.target.value)}
        />
      </div>
      <button onClick={searchHandler}>search</button>
    </div>
  )
}

export default SearchBar
